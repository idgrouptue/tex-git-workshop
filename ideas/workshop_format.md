# Git + Latex + Research methods workshop
 


## When?
2 sessions x 2h, 02-03/12/2014
 
## Schedule

### Day 1

15 min

* Install Git
* Install Latex
* Create Bitbucket account
* Install SmartGit
* Clone the repository: https://bitbucket.org/idgrouptue/tex-git-workshop

30 min 
 
* Git commands
	* Clone
	* Add
	* Commit
	* Pull
	* Push
	* Staging area
	* Branch
	* Merge
	* Rebase
	* Log
	* Tags
	* Checkout
	* Conflicts
	* Config
	* Gitignore
	* Merge with Annotated Tags

15 min

* [tryGit](http://try.github.com) 

15min COFFEE BREAK

45min

* LaTeX
	* Template
	* Hello world
	* Sections
	* Title page/abstract
	* Math environment


###Homework 

* Port your own paper to LaTeX
* Commit it to the GIT repo using a branch of your own
 
### Day 2

* LaTeX continued
	* Images
	* Subimage
	* Tables
	* Multirow/Multicolumn
	* Three-part table
	* Longtable
	* Landscape
	* References (Reference manager exporting to .bib)
	* todo notes
	* Change to different style (e.g. from default to IEEE or elsarticle)
	* TIKS
