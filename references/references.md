# Interesting references

## Why use Git?

* [Git for Scientists: A Tutorial](http://nyuccl.org/pages/GitTutorial/)
* [Git/Github: A primer for researchers](http://datapub.cdlib.org/2014/05/05/github-a-primer-for-researchers/) 
* [Git can facilitate greater reproducibility and increased transparency in science. Source Code for Biology and Medicine 2013, 8:7  doi:10.1186/1751-0473-8-7](http://www.scfbm.org/content/8/1/7/abstract)

##Git tutorials

* [tryGit](http://try.github.com)
* [Atlassian Git Tutorials](https://www.atlassian.com/git/tutorials/)
* [A Visual Git Reference](http://marklodato.github.io/visual-git-guide/index-en.html)
* [Think like a git](http://think-like-a-git.net/)
* [git Book](http://git-scm.com/book/en/v2)

## Wikibooks examples and extended documentation

* _Add things here_

